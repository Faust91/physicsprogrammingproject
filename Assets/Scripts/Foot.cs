using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum FootEnum
{
    NONE, LEFT, RIGHT
}

public class Foot : MonoBehaviour
{
    [Header("Appearance")]
    [SerializeField] private Color minColor = Color.green;
    [SerializeField] private Color midColor = Color.yellow;
    [SerializeField] private Color maxColor = Color.red;
    [SerializeField] private Color disabledColor = Color.gray;

    private Rigidbody rb;

    [Header("Stats")]
    private FootEnum footEnum;

    private Material material;
    private Skateboard skateboardSystem;

    private int skateboardLayerMask;
    private int notSkateboardLayerMask;

    [SerializeField] private bool somethingBelowMe = true;
    [SerializeField] private bool inSkaterRange = true;

    public void Init(FootEnum leftOrRight)
    {
        footEnum = leftOrRight;
    }

    private void Awake()
    {
        material = GetComponent<MeshRenderer>().material;
        rb = GetComponent<Rigidbody>();
    }

    private void Start()
    {
        skateboardSystem = FindObjectOfType<Skateboard>();
        skateboardLayerMask = LayerMask.NameToLayer("Skateboard");
        notSkateboardLayerMask = ~skateboardLayerMask;
    }

    private void Update()
    {
        UpdateFootColor();
    }

    public bool IsSomethingBelowMe()
    {
        return somethingBelowMe;
    }

    public bool IsInRange()
    {
        return inSkaterRange;
    }

    public void EvaluateForce(float amount, Transform socket)
    {
        RaycastHit hit;
        Physics.Raycast(transform.position, -transform.up, out hit, 10f, notSkateboardLayerMask);
        somethingBelowMe = hit.collider;
        if (inSkaterRange && somethingBelowMe && amount > 0.0f)
        {
            rb.AddForce(-socket.up * amount, ForceMode.Force);
        }
        else
        {
            ResetFoot(socket);
        }
    }

    public void ResetFoot(Transform socket)
    {
        transform.position = socket.position;
        transform.rotation = socket.rotation;
        rb.velocity = new Vector3(0, 0, 0);
        rb.angularVelocity = new Vector3(0, 0, 0);
    }

    public void SetInRange()
    {
        inSkaterRange = true;
    }

    public void SetOutOfRange()
    {
        inSkaterRange = false;
    }

    private void UpdateFootColor()
    {
        if (somethingBelowMe)
        {
            if (footEnum == FootEnum.LEFT)
            {
                SetColor(skateboardSystem.LeftFootForce);
            }
            else if (footEnum == FootEnum.RIGHT)
            {
                SetColor(skateboardSystem.RightFootForce);
            }
        }
        else
        {
            SetDisabledColor();
        }
    }

    private void SetDisabledColor()
    {
        material.color = disabledColor;
    }

    private void SetColor(float t)
    {
        if (t < 0.5f)
        {
            material.color = Color.Lerp(minColor, midColor, 2.0f * t);
        }
        else
        {
            material.color = Color.Lerp(midColor, maxColor, 2.0f * t - 1.0f);
        }
    }

}
