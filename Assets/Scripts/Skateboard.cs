using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Skateboard : MonoBehaviour
{
    [Header("Arena")]
    [SerializeField] private Transform arena;
    [SerializeField] private SphereCollider skater;

    [Header("Skateboard Setup")]
    [SerializeField] private Transform leftSocket;
    [SerializeField] private Transform rightSocket;

    private Vector3 leftSocketOriginalPosition;
    private Vector3 rightSocketOriginalPosition;
    private Quaternion leftSocketOriginalRotation;
    private Quaternion rightSocketOriginalRotation;

    [Header("Prefabs")]
    [SerializeField] private GameObject footPrefab;

    [Header("Settings")]
    [SerializeField] private float maxForceAmount = 500.0f;
    [SerializeField] private float horizontalRoom = 0.25f;
    [SerializeField] private float verticalRoom = 0.15f;

    private GameObject leftFoot;
    private GameObject rightFoot;

    private Rigidbody leftFootRB;
    private Rigidbody rightFootRB;

    private Foot leftFootScript;
    private Foot rightFootScript;

    [Header("Stats")]
    [SerializeField] [Range(0.0f, 1.0f)] private float leftFootDown = 0.0f;
    [SerializeField] [Range(0.0f, 1.0f)] private float rightFootDown = 0.0f;
    [SerializeField] [Range(-1.0f, 1.0f)] private float leftX = 0.0f;
    [SerializeField] [Range(-1.0f, 1.0f)] private float leftY = 0.0f;
    [SerializeField] [Range(-1.0f, 1.0f)] private float rightX = 0.0f;
    [SerializeField] [Range(-1.0f, 1.0f)] private float rightY = 0.0f;

    public Vector3 LeftFootPosition
    {
        get { return new Vector3(leftX, leftY, 0.0f); }
        set
        {
            leftX = value.x;
            leftY = value.y;
        }
    }

    public Vector3 RightFootPosition
    {
        get { return new Vector3(rightX, rightY, 0.0f); }
        set
        {
            rightX = value.x;
            rightY = value.y;
        }
    }

    public float LeftFootForce
    {
        get { return leftFootDown; }
        set { leftFootDown = value; }
    }

    public float RightFootForce
    {
        get { return rightFootDown; }
        set { rightFootDown = value; }
    }

    private void Awake()
    {
        leftSocketOriginalPosition = leftSocket.localPosition;
        rightSocketOriginalPosition = rightSocket.localPosition;
        leftSocketOriginalRotation = leftSocket.localRotation;
        rightSocketOriginalRotation = rightSocket.localRotation;
    }

    private void Start()
    {
        leftFoot = Instantiate(footPrefab, leftSocket.position, Quaternion.identity, arena);
        leftFoot.name = "LeftFoot";
        leftFoot.GetComponent<Foot>().Init(FootEnum.LEFT);
        rightFoot = Instantiate(footPrefab, rightSocket.position, Quaternion.identity, arena);
        rightFoot.name = "RightFoot";
        rightFoot.GetComponent<Foot>().Init(FootEnum.RIGHT);
        leftFootRB = leftFoot.GetComponent<Rigidbody>();
        rightFootRB = rightFoot.GetComponent<Rigidbody>();
        leftFootScript = leftFoot.GetComponent<Foot>();
        rightFootScript = rightFoot.GetComponent<Foot>();
        SpringJoint leftJoint = leftSocket.GetComponent<SpringJoint>();
        SpringJoint rightJoint = rightSocket.GetComponent<SpringJoint>();
        leftJoint.connectedBody = leftFootRB;
        rightJoint.connectedBody = rightFootRB;
    }

    public void ResetSkateboard()
    {
        transform.position = arena.position;
        transform.rotation = arena.rotation;
        gameObject.GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);
        gameObject.GetComponent<Rigidbody>().angularVelocity = new Vector3(0, 0, 0);
        ResetSockets();
        leftFootScript.ResetFoot(leftSocket);
        rightFootScript.ResetFoot(rightSocket);
    }

    private void ResetSockets()
    {
        leftSocket.localPosition = leftSocketOriginalPosition;
        leftSocket.localRotation = leftSocketOriginalRotation;
        rightSocket.localPosition = rightSocketOriginalPosition;
        rightSocket.localRotation = rightSocketOriginalRotation;
    }

    private void FixedUpdate()
    {
        ResetSockets();

        leftSocket.position += leftSocket.right * leftX * horizontalRoom;
        leftSocket.position += leftSocket.forward * leftY * verticalRoom;
        rightSocket.position += rightSocket.right * rightX * horizontalRoom;
        rightSocket.position += rightSocket.forward * rightY * verticalRoom;

        leftFootScript.EvaluateForce(leftFootDown * maxForceAmount, leftSocket);
        rightFootScript.EvaluateForce(rightFootDown * maxForceAmount, rightSocket);
    }

}
