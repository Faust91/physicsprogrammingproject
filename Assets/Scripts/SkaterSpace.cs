using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkaterSpace : MonoBehaviour
{

    private void OnTriggerEnter(Collider other)
    {
        other.gameObject.GetComponent<Foot>().SetInRange();
    }

    private void OnTriggerExit(Collider other)
    {
        other.gameObject.GetComponent<Foot>().SetOutOfRange();
    }
}
