using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerInput : MonoBehaviour, SkateboardControls.ISkateboardActions
{
    [SerializeField] private GameObject skateboard;
    private Skateboard skateboardSystem;

    private void Start()
    {
        skateboardSystem = skateboard.GetComponent<Skateboard>();
    }

    public void SetSkateboardSystem(Skateboard skateboardSystem)
    {
        this.skateboardSystem = skateboardSystem;
    }

    public Skateboard GetSkateboardSystem()
    {
        return skateboardSystem;
    }

    public void SetLeftFootPosition(Vector2 leftPos)
    {
        skateboardSystem.LeftFootPosition = leftPos;
    }

    public void SetRightFootPosition(Vector2 rightPos)
    {
        skateboardSystem.RightFootPosition = rightPos;
    }

    public void SetLeftForce(float amount)
    {
        skateboardSystem.LeftFootForce = amount;
    }

    public void SetRightForce(float amount)
    {
        skateboardSystem.RightFootForce = amount;
    }

    public void OnLeftFootPush(InputAction.CallbackContext context)
    {
        float amount = context.ReadValue<float>();
        SetLeftForce(amount);
    }

    public void OnRightFootPush(InputAction.CallbackContext context)
    {
        float amount = context.ReadValue<float>();
        SetRightForce(amount);
    }

    public void OnLeftFootMove(InputAction.CallbackContext context)
    {
        Vector2 movement = context.ReadValue<Vector2>();
        SetLeftFootPosition(movement);
    }

    public void OnRightFootMove(InputAction.CallbackContext context)
    {
        Vector2 movement = context.ReadValue<Vector2>();
        SetRightFootPosition(movement);
    }

    public void OnResetPosition(InputAction.CallbackContext context)
    {
        skateboard.GetComponent<Skateboard>().ResetSkateboard();
    }
}
